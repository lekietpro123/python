# nhập số giây từ bàn phím
a = int(input('Nhập số giây:'))
# lấy phần nguyên của 3600 là ta có được số giờ do 1 giờ = 3600 giây
b = a // 3600
# lấy phần nguyên của 60 là ta có được 1 phút = 60 giây
c = a // 60
# khi tính được số phút, ta tiếp tục lấy số a trừ cho số phút đó là ta còn được số giây còn lại
a = a - c*60
# xuất dữ liệu số giờ, phút giây, ra màn hình
print ('Số giờ, phút, giây là:{0} giờ, {1} phút, {2} giây'. format(b, c, a))
