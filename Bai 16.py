# ax^2 + bx + c = 0
#giải bất phương trình
from math import *
a = int (input(' hãy nhập a:'))
b = int (input(' hãy nhập b:'))
c = int (input(' hãy nhập c:'))
if a == 0:
    print ('phương trình có nghiệm bậc nhất bx + c = 0')
else:
    delta  = b*b - (4*a*c)
    if delta < 0:
        print ('phương trình vô nghiệm')
    if delta == 0:
        print (' phương trình có 1 nghiệm:', -b/(2*a))
    if delta > 0:
        print ('phương trình có 2 nghiệm phân biệt')
        x1 = ( -b - sqrt (delta)) / 2*a
        x2 = ( -b + sqrt (delta)) / 2*a
        print ('nghiệm thứ nhất là:', x1 )
        print ('nghiệm thứ hai là:', x2 )


