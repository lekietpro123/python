# dùng hàm nhập các công thức tính chu vi và diện tích hình tam giác từ bàn phím
def Chuvi (a,b,c):
    return (a+b+c)
def DienTich(d,e):
    return (d+e)/2
# nhập số liệu để tính
print ('Nhập số đo ba cạnh tam giác a,b,c:')
a,b,c = map (int,input().split())
print ('Nhập độ cao và cạnh huyền của tam giác:')
d,e = map (int,input().split())
cv = Chuvi (a,b,c)
dt = DienTich (d,e)
# xuất kết quả
print ('Chu vi hình tam giác là:', cv )
print ('Diện tích hình tam giác là:', dt )

# 2  nhập hàm công thức tính chu vi và diên tích của hình vuông từ bàn phím
def ChuVi (a):
    return (a*4)
def DienTich (a):
    return (a*a)
# nhập só liệu để tính
a = int(input('Nhập cạnh của hình vuông:'))
cv = ChuVi (a)
dt = DienTich (a)
# xuất dữ liệu ra màn hình
print ('Chu vi hình vuông là:', cv )
print ('Diện tích hình vuông là:', dt )

# 3 nhập hàm công thức tính chu vi và diện tích hình chữ nhật từ bàn phím
def ChuVi(a,b):
    return (a+b)*2
def DienTich(a,b):
    return (a*b)
# nhập số liệu
print ('Nhập cạnh thứ nhất và thứ hai của hình chữ nhật:')
a,b = map (int,input().split())
cv = ChuVi (a,b)
dt = DienTich (a,b)
# xuất dữ liêu ra màn hình
print ('Chu vi hình chữ nhật là:', cv )
print ('Diện tích hình chữ nhật là:', dt )

# 4 nhập công thức tính chu vi và diện tích của hình tròn từ bàn phím
def ChuVi (a):
    return (a * 2 * 3.14)
def DienTich (a):
    return (a * a * 3.14)
# nhập só liệu
a = int (input('Nhập bán kính của hình tròn a:'))
cv = ChuVi (a)
dt = DienTich (a)
# xuất dữ liêu ra màn hình
print ('Chu vi hình tròn là:', cv )
print ('Diện tích hình tròn là:', dt )


