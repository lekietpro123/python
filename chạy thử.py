# tính chu vi và diện tích hình chữ nhật
def ChuVi(a,b):
    return (a + b) * 2
def DienTich(a,b):
    return (a * b)
print ('Mời bạn nhập chiều dài a và chiều rộng b:', end=' ')
a, b = map(int, input().split())
cv = ChuVi(a,b)
dt = DienTich(a,b)
print ('Chu vi là:', cv, '\nDiện tích là:', dt)
# tính chu vi và diện tích hình tam giác
def ChuVi(a,b,c):
    return (a + b + c)
def DienTich(d,h):
    return (d * h) / 2
print ('Mời bạn nhập số đo 3 cạnh a,b và c:', end=' ')
a, b, c = map(int, input().split())
print ('Mời bạn nhập chiều cao h và cạnh huyền d:', end=' ')
d, h = map(int, input().split())
cv = ChuVi(a,b,c)
dt = DienTich(d,h)
print ('Chu vi là:', cv, '\n Diện tích là:', dt)
# tính chu vi và diện tích hình tròn
def ChuVi(a):
    return (a * 3.14 * 2)
def DienTich(a):
    return (a ** 2 * 3.14)
print ('Mời bạn nhập bán kính:', end=' ')
a = int(input())
cv = ChuVi(a)
dt = DienTich(a)
print ('Chu Vi là:', cv, '\nDiện tích là:', dt )
# tính chu vi và diện tích hình vuông
def ChuVi(a):
    return (a * 4)
def DienTich(a):
    return (a * a)
print ('Mời bạn nhập cạnh:', end=' ')
a = int(input())
cv = ChuVi(a)
dt = DienTich(a)
print ('Chu vi là:', cv, '\n Diện tích là:', dt)





