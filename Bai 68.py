#Viết chương trình phát sinh ngẫu nhiên mảng một chiều các số nguyên âm.
import random
def NhapMang(a):
    print ('Mời bạn nhập số phần tử của mảng:',end=' ')
    n = int(input())
    for i in range (n):
        x = random.randint(-100,-50)
        a.append (x)
def XuatMang(a):
    print ('Các phần tử nguyên âm của mảng a là:', end=' ')
    for i in range (len(a)):
        print (a[i], end=' ')
a=[]
NhapMang(a)
XuatMang(a)

