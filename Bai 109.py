# viết hàm sắp xếp mảng theo thứ tự giảm dần
import random
def NhapMang(a):
    print ('Mời bạn nhập các giá trị của mảng:', end=' ')
    n = int(input())
    for i in range (n):
        x = random.randint(1,100)
        a.append(x)
def XuatMang(a):
    print ('Các giá trị giảm dần của mảng là:')
    for i in range (len(a)):
        print (a[i], end=' ')

a=[]
NhapMang(a)
a.sort(reverse=True)
XuatMang(a)