# nhập số nguyên dương n gồm k chữ số ( 0 < k <= 5 )
# tính tổng ước số của n
# ví dụ: n = 6
        # n = 1 + 2 + 3 + 6 = 12
def TongUS (n):
    t = 0
    for i in range (1,n+1):
        if n % i == 0:
            t = t + i
    return t

print ('nhập n:')
n = int(input())
kq = TongUS(n)
print ('Tổng các ước số của n là:', kq )