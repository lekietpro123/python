# tính S(n) = 1 + 1/2! + 1/3!+...+1/n! với n > 0
n = int (input('Nhập n:'))
t = 0
s = 1
for i in range (1,n+1):
   s *= i
   t += 1/s
print ('Kết quả là:', t )