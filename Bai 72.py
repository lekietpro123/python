#Viết chương trình nhập vào mảng một chiều các số nguyên
#xuất ra các phần tử chẵn nhỏ hơn 20.
def NhapMang(a):
    print ('Mời bạn nhập các phần tử mảng:', end=' ')
    n = int(input())
    for i in range (n):
        x = int(input())
        a.append (x)
def XuatMang(a):
    print('Các giá trị của mảng a là:')
    for i in range (len(a)):
        print (a[i], end=' ')
def XuatChanCoDK(a):
    print ('\nCác phần tử chẵn và nhỏ hơn 20 là:')
    for i in range (len(a)):
        if a[i] % 2 == 0 and a[i] < 20:
            print (a[i], end=' ')
a=[]
NhapMang(a)
XuatMang(a)
XuatChanCoDK(a)
