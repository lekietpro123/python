#Viết hàm tìm vị trí phần tử âm đầu tiên trong mảng. Nếu không có phần tử âm trả về -1
def NhapMang(a):
    print ('Nhập các phần tử mảng:', end=' ')
    n = int (input())
    for i in range (n):
        x = int(input())
        a.append (x)
def XuatMang(a):
    for i in range(len(a)):
        print (a[i], end=' ')

#tìm vị trí âm đầu tiên trong mảng nếu không trả về - 1
def VTAmDau(a):
    for i in range(len(a)):
        if a[i] < 0:
            return i
    return -1
a=[]
NhapMang(a)
XuatMang(a)
kq = VTAmDau(a)
if kq == -1:
    print ('Không có số âm nào trong mảng')
else:
    print ('Vị trí số âm đầu tiên là:', kq )

