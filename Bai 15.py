#giải và biện luận phưng trình: ax + b = 0
# nếu a = 0
# nếu b = 0 thì phương trình vô số nghiệm
# nếu b khác 0 thì phương trình vô nghiệm
# nếu a khác 0 thì phương trình có nghiệm duy nhất -b/a
a = int (input('hãy nhập a:'))
b = int (input('hãy nhập b:'))
if a == 0:
    if b == 0:
        print ('phương trình vô số nghiệm')
    else:
        print ('phương trình vô nghiệm')
else:
    print ('phương trình có nghiệm duy nhất', -b/a )