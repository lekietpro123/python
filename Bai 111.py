#Viết hàm sắp xếp các phần tử lẻ tăng dần
def NhapMang(a):
    print ('Mời bạn nhập giá trị của mảng:', end=' ')
    n = int(input())
    for i in range(n):
        x = int(input())
        a.append(x)
def XuatMang(a):
    print ('Các giá trị có trong mảng là:')
    for i in range (len(a)):
        print (a[i], end=' ')
def XuatLe(a):
    print ('\n Các phần tử lẻ tăng dần trong mảng là:')
    for i in range (len(a)):
        if a[i] % 2!= 0:
            print (a[i], end=' ')
a=[]
NhapMang(a)
XuatMang(a)
a.sort(reverse=False)
XuatLe(a)
