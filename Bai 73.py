# nhập vào mảng một chiều các số nguyên
# Xuất ra màn hình là các số nguyên tố
def NhapMang(a):
    print ('Nhập số phần tử của mảng:', end=' ')
    n= int(input())
    for i in range (n):
        x = int (input())
        a.append (x)
def XuatMang(a):
    print('Các giá trị của mảng:')
    for i in range (len(a)):
        print (a[i], end=' ')
# xuất các phần tử là các số nguyên tố
def KTNguyenTo (k):
    dem = 0
    for i in range (1,k+1):
        if k % i == 0:
            dem = dem + 1
    if dem == 2:
        return True
    else:
        return False
a=[]
NhapMang(a)
XuatMang(a)
print ('\n Các số nguyên tố có trong mảng là:')
for i in range (len(a)):
    if KTNguyenTo (a[i]) == True:
        print (a[i], end=' ')

