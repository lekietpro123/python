# viết chương trình khởi tạo các giá trị các phần tử là 0 cho mảng một chiều các số nguyên gồm n phần tử
def NhapMang(a):
    print ('Mời bạn nhập các giá trị phần tử mảng:', end=' ')
    n = int(input())
    for i in range(n):
        x = int(input())
        a.append(0)
def XuatMang(a):
    print('Các giá trị phần tử là 0 của mảng a là:')
    for i in range (len(a)):
        print (a[i], end=' ')

a=[]
NhapMang(a)
XuatMang(a)
