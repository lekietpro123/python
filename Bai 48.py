#nhập số nguyên dương n ( 0 <= n < 1000 ) và in ra các đọc của n.
#vd:105 in ra là một trăm lẻ năm
n = int (input('Nhập n:'))
def KTNhapn(n):
    if n > 99 and n < 1000:
        return True
    else:
        return False
def DocTram(tram):
    switcher = {
        1: 'một trăm',
        2: 'hai trăm',
        3: 'ba trăm',
        4: 'bốn trăm',
        5: 'năm trăm',
        6: 'sáu trăm',
        7: 'bảy trăm',
        8: 'tám trăm',
        9: 'chín trăm',
    }
    return switcher.get(tram,'số hàng trăm bị lỗi')
def DocChuc(chuc):
    switcher = {
        0: 'lẻ',
        1: 'mười',
        2: 'hai mươi',
        3: 'ba mươi',
        4: 'bốn mươi',
        5: 'năm mươi',
        6: 'sáu mươi',
        7: 'bảy mươi',
        8: 'tám mươi',
        9: 'chín mươi',
    }
    return switcher.get(chuc,'số hàng chục bị lỗi')
def DocDonVi(donvi):
    switcher = {
        0: ' ',
        1: 'mốt',
        2: 'hai',
        3: 'ba',
        4: 'bốn',
        5: 'năm',
        6: 'sáu',
        7: 'bảy',
        8: 'tám',
        9: 'chín',
    }
    return switcher.get(donvi,'số hàng đơn vị bị lỗi')
tram = n // 100
chuc = n % 100 // 10
donvi = n % 100 % 10
if donvi == 0 and chuc == 0:
    print (DocTram(tram))
else:
    print (DocTram(tram), DocChuc(chuc), DocDonVi(donvi))