# viết hàm sắp xếp các phần tử chẵn giảm dần
def NhapMang(a):
    print ('Mời bạn nhập các giá trị mảng:', end=' ')
    n = int(input())
    for i in range(n):
        x = int(input())
        a.append(x)
def XuatMang(a):
    print ('Các giá trị trong mảng là:')
    for i in range (len(a)):
        print(a[i], end=' ')
def XuatChan(a):
    print ('Các phần tử chẵn giảm dần là:')
    for i in range (len(a)):
        if a[i] % 2== 0:
            print (a[i], end=' ')
a=[]
NhapMang(a)
XuatMang(a)
a.sort(reverse=True)
XuatChan(a)

