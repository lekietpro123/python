# nhập a,b: thực hiện phép tính tổng, tích, hiệu, thương bằng hàm

#thực hiện hàm
from math import *
def Tong (a,b):
    return (a+b)
def Tich (a,b):
    return (a*b)
def Hieu (a,b):
    return (a-b)
def Thuong (a,b):
    return (a/b)
# nhập số liệu
print ('Mời bạn nhập 2 số a và b:')
a,b = map (int,input().split())
tong = Tong (a,b)
tich = Tich (a,b)
hieu = Hieu (a,b)
thuong = Thuong (a,b)
# xuất kết quả
print ('Tổng của a + b là:', tong )
print ('Tích của a * b là:', tich )
print ('Hiệu của a - b là:', hieu )
print ('Thương của a / b là:', thuong )

