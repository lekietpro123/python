#Viết chương trình phát sinh ngẫu nhiên mảng một chiều các số nguyên
#sao cho mảng có thứ tự tăng dần (không sắp xếp)
import random
def NhapMang(a):
    print ('Mời bạn nhập phần tử các số nguyên tố:', end=' ')
    n = int(input())
    for i in range (n):
        x = random.randint(1,100)
        a.append(x)
def XuatMang(a):
    print ('Các giá trị tăng dần của mảng là:', end=' ')
    for i in range (len(a)):
        print (a[i], end=' ')

a=[]
NhapMang(a)
a.sort(reverse=False)
XuatMang(a)
