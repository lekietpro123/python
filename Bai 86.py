#Viết hàm tìm số lẻ lớn nhất có trong mảng, nếu không tồn tại số lẻ hàm trả về -1
def NhapMang(a):
    print ('Mời bạn nhập các giá trị mảng:', end=' ')
    n = int(input())
    for i in range (n):
        x = int(input())
        a.append (x)
def XuatMang(a):
    print ('Các giá trị mảng là:')
    for i in range (len(a)):
        print (a[i], end=' ')
def TimLeDauTien (a):
    for i in range (len(a)):
        if a[i] % 2!= 0:
            return a[i]
    return -1
def TimLeLN(a):
    lc = TimLeDauTien(a)
    if lc == -1:
        print ('Không có số lẻ nào trog mảng:')
    for i in range (len(a)):
        if a[i] % 2!= 0 and a[i] > lc:
                lc = a[i]
    return lc
a=[]
NhapMang(a)
XuatMang(a)
kq=TimLeLN(a)
print ('\nSố lẻ lớn nhất là:', kq)
