# nhập số nguyên dương n gồm k chữ số ( 0 < k <= 5 )
# kiểm tra xem các chữ số của n có toàn lẻ hay toàn chẵn không
def KTToanLe(n):
    while (n!=0):
        donvi=n%10
        if donvi%2==0:
            return False
        n=n//10
    return True

def KTToanChan(n):
    while(n!=0):
        donvi=n%10
        if donvi%2!=0:
            return False
        n=n//10
    return True
print(' nhap n ')
n=int(input())
kq=KTToanLe(n)
if kq==True:
    print('toan le')
kq=KTToanChan(n)
if kq==True:
    print(' toan chan')