# nhập số đo nhiệt độ Fahrenheit từ bàn phím
print ('Nhập số đo nhiệt độ Fahrenheit: ')
a = float(input())

# công thức tính số đo nhiệt độ Celsius từ nhiệt độ Fahreneit
b = 5 / 9 * (a - 32)

# xuất dữ liệu Celsius đã tính được ra màn hình
print ('Số đo nhiệt độ Celsius tương ứng: 5 / 9 * ({0} - 32) = {1} độ C'.format(a, b))