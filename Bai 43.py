#tính diện tích và chu vi hình chữ nhật với chiều dài và chiều rộng được nhập từ bàn phím
def ChuVi (a,b):
    return (a+b)*2
def DienTich(a,b):
    return (a*b)
# nhập chiểu dài và chiều rộng
d = int (input('Nhập chiều dài:'))
r = int (input('Nhập chiều rộng:'))
# tính chu vi
cv = ChuVi(d,r)
print ('Chu vi hình chữ nhật là:', cv)
#tính diện tích
dt = DienTich(d,r)
print ('Diện tích hình chữ nhật là:', dt)